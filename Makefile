#
# Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
#
# This file is part of 3DAirQualityPredictionPilot.
#
# 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with 3DAirQualityPredictionPilot.
# If not, see <https://www.gnu.org/licenses/>.

DEST    = 3dAirQualityPredictionPilot

all: $(DEST)

include $(UNICORNLIBDIR)/Makefile.lib
include config.mk

MYUFC2OBJECTS = $(SRCDIR)/NO.o \
				$(SRCDIR)/NO2.o \
				$(SRCDIR)/O3.o
MYOBJECTS = 3dAirQualityPredictionPilot.o
AQPOBJECTS = $(SRCDIR)/AQPSolver.o \
			 $(SRCDIR)/EmissionData.o \
			 $(SRCDIR)/EmissionSource.o \
			 $(SRCDIR)/NO2Solver.o \
			 $(SRCDIR)/NOSolver.o \
			 $(SRCDIR)/O3Solver.o \
			 $(SRCDIR)/StreetMappingData.o \
			 $(SRCDIR)/StreetsData.o \
			 $(SRCDIR)/WindData.o \
			 $(SRCDIR)/WindInflow.o
HEADERDEPS += $(SRCDIR)/NOProblem.h \
			  $(SRCDIR)/NO2Problem.h \
			  $(SRCDIR)/O3Problem.h \
			  $(AQPOBJECTS:.o=.h)

$(AQPOBJECTS) : $(UNICORNOBJECTS) $(MYUFC2OBJECTS)
$(MYOBJECTS) : $(AQPOBJECTS) $(HEADERDEPS)

clean:
	-rm -f $(MYOBJECTS) $(DEST) $(AQPOBJECTS) $(MYUFC2OBJECTS) $(MYUFC2OBJECTS:.o=.cpp) $(MYUFC2OBJECTS:.o=.h)

$(DEST): $(MYOBJECTS)
	$(CXX) -o $@ $(UNICORNOBJECTS) $(AQPOBJECTS) $(MYUFC2OBJECTS) $(MYOBJECTS) $(CFLAGS) $(LIBS)
