/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "AQPSolver.h"
#include "EmissionSource.h"
#include "WindInflow.h"
#include "WindData.h"
#include "StreetsData.h"
#include "StreetMappingData.h"

#include <dolfin.h>

using namespace dolfin;

int main(int argc, char* argv[])
{
    dolfin_init(argc,argv);
    {
        dolfin_set("output destination","silent");
        if(dolfin::MPI::processNumber() == 0)
            dolfin_set("output destination","terminal");

        if(!ParameterSystem::parameters.defined("adapt_percent"))
            dolfin_add("adapt_percent", 10.0);
        if(!ParameterSystem::parameters.defined("nu"))
            dolfin_add("nu", 0.0);
        if(!ParameterSystem::parameters.defined("alpha"))
            dolfin_add("alpha", 0.0);
        if(!ParameterSystem::parameters.defined("cfl_target"))
            dolfin_add("cfl_target", 10.0);
        if(!ParameterSystem::parameters.defined("trip_factor"))
            dolfin_add("trip_factor", 0.0);
        if(!ParameterSystem::parameters.defined("T"))
            dolfin_add("T", 20.0);
        if(!ParameterSystem::parameters.defined("discrete_tolerance"))
            dolfin_add("discrete_tolerance", 0.01);
        File paramfile("parameters.xml");
        paramfile >> ParameterSystem::parameters;
        real cfl_target = (real) dolfin_get("cfl_target");
        real nu = 0;

        std::string config_file;
        std::string meshPrefix;
        std::string inputFilesPrefix;
        std::string windFileX;
        std::string windFileY;
        std::string segmentsFile;
        std::string emissionFile;
        real NOProdPerc;
        real NO2ProdPerc;
        real boundaryAirPollution;
        real NOInitialConcentration;
        real NO2InitialConcentration;
        real O3InitialConcentration;
        real NOBackground;
        real NO2Background;
        real O3Background;
        int noSamples;
        real dt;
        real tFinal;

        po::options_description generic("Generic options");
        generic.add_options()
            ("config,c", po::value<std::string>(&config_file),
             "Configuration file");

        po::options_description config("Configuration");
        config.add_options()
            ("meshPrefix",
                 po::value<std::string>(&meshPrefix)->default_value(std::string()))
            ("inputFilesPrefix",
                 po::value<std::string>(&inputFilesPrefix)->default_value(std::string()))
            ("windFileX",
                 po::value<std::string>(&windFileX)->default_value(std::string()))
            ("windFileY",
                 po::value<std::string>(&windFileY)->default_value(std::string()))
            ("segmentsFile",
                 po::value<std::string>(&segmentsFile)->default_value(std::string()))
            ("emissionFile",
                 po::value<std::string>(&emissionFile)->default_value(std::string()))
            ("NOProdPerc",
                 po::value<real>(&NOProdPerc))
            ("NO2ProdPerc",
                 po::value<real>(&NO2ProdPerc))
            ("boundaryAirPollution",
                 po::value<real>(&boundaryAirPollution))
            ("NOInitialConcentration",
                 po::value<real>(&NOInitialConcentration))
            ("NO2InitialConcentration",
                 po::value<real>(&NO2InitialConcentration))
            ("O3InitialConcentration",
                 po::value<real>(&O3InitialConcentration))
            ("NOBackground",
                 po::value<real>(&NOBackground))
            ("NO2Background",
                 po::value<real>(&NO2Background))
            ("O3Background",
                 po::value<real>(&O3Background))
            ("dt",
                 po::value<real>(&dt))
            ("tFinal",
                 po::value<real>(&tFinal))
            ("noSamples",
                 po::value<int>(&noSamples))
            ;

        po::options_description cmdline_options;
        cmdline_options.add(generic);

        po::options_description config_file_options;
        config_file_options.add(config);

        po::variables_map vm;
        store(po::command_line_parser(argc, argv).
                options(cmdline_options).run(), vm);
        notify(vm);

        std::ifstream ifs(config_file.c_str());
        if (!ifs)
        {
            std::cout << "Cannot open config file: " << config_file << "\n";
            return 0;
        }
        else
        {
            store(parse_config_file(ifs, config_file_options), vm);
            notify(vm);
        }

        // Create mesh
        Mesh mesh(meshPrefix + "Domain.xml");

        dolfin_set("Mesh read in serial", true);

        // Create boundary conditions
        Mesh airMesh(meshPrefix + "Air.stl");
        Mesh groundMesh(meshPrefix + "Ground.stl");

        dolfin_set("Mesh read in serial", false);

        SubDomain air(airMesh);
        SubDomain ground(groundMesh);

        dolfin_set("SubDomain Geometrical Tolerance", 1.0);
        dolfin_set("SubDomain GTS Tolerance", 1e-2);

        // Parameters for Navier-Stokes
        WindData windData(inputFilesPrefix + windFileX,
                          inputFilesPrefix + windFileY);
        WindInflow inflow(mesh,windData);
        Function u_i0(mesh,Array<real>(0,0,0));
        Function f(mesh,Array<real>(0,0,0));
        Function outflow(mesh,0);

        OutputQuantityList outputQuantities;
        MeshFunction<uint> sm(mesh,mesh.topology().dim()-1);
        sm = 1;
        ground.mark(sm,0);

        DirichletBCList dbcs_m;
        dbcs_m.push_back(std::make_pair(&air,&inflow));

        DirichletBCList dbcs_c;
        dbcs_c.push_back(std::make_pair(&air,&outflow));

        DirichletBCList dbcs_dm;

        // Parameters for pollutants
        StreetsData streetsData(inputFilesPrefix + segmentsFile);
        StreetMappingData streetMappingData(mesh, ground, streetsData);
        EmissionData emissionData(inputFilesPrefix + emissionFile);
        EmissionSource NOv(mesh,streetMappingData,emissionData,streetsData,NOProdPerc);
        EmissionSource NO2v(mesh,streetMappingData,emissionData,streetsData,NO2ProdPerc);
        Function airPollution(mesh,boundaryAirPollution);
        Function NOInitial(mesh,NOInitialConcentration);
        Function NO2Initial(mesh,NOInitialConcentration);
        Function O3Initial(mesh,O3InitialConcentration);
        Function NOb(mesh,NOBackground);
        Function NO2b(mesh,NO2Background);
        Function O3b(mesh,O3Background);

        DirichletBCList dbcs_s;
        dbcs_s.push_back(std::make_pair(&air,&airPollution));

        AQPSolver solver(
                mesh,
                dbcs_m,
                dbcs_c,
                dbcs_dm,
                &u_i0,
                outputQuantities,
                &sm,
                &f,
                cfl_target,
                nu,
                &NOb,
                &NOv,
                &NO2b,
                &NO2v,
                &O3b,
                &NOInitial,
                &NO2Initial,
                &O3Initial,
                dbcs_s
                );
        inflow.setSolver(&solver);
        NOv.setSolver(&solver);
        NO2v.setSolver(&solver);
        solver.setDt(dt);
        solver.setNoSamples(noSamples);
        solver.setTFinal(tFinal);
        solver.run();

#warning "TODO: decide on linearization for pollutants"
    }

    dolfin_finalize();
    return 0;
}
