#
# Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
#
# This file is part of 3DAirQualityPredictionPilot.
#
# 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with 3DAirQualityPredictionPilot.
# If not, see <https://www.gnu.org/licenses/>.

export AQPDIR = $(dir $(lastword $(MAKEFILE_LIST)))
export SRCDIR = $(AQPDIR)/src/

export CFLAGS  += -I$(SRCDIR) -std=c++98 -O3
export LIBS    += -L$(SRCDIR) -lboost_program_options
export CXX     ?= `pkg-config --variable=compiler dolfin`
