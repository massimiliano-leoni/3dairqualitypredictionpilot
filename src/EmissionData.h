/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EMISSIONDATA_H
#define EMISSIONDATA_H

#include <string>
#include <dolfin.h>

class EmissionData
{
public:
    EmissionData (std::string f);
    virtual ~EmissionData () {};

    dolfin::real eval(const dolfin::uint id, const dolfin::real t) const;

private:
    std::vector<dolfin::real> timePoints;
#warning "Should use a contiguous space?"
    std::map<dolfin::uint,std::vector<dolfin::real> > data;
};

#endif /* end of include guard: EMISSIONDATA_H */
