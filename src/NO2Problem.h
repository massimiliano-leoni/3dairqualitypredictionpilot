/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NO2PROBLEM_H
#define NO2PROBLEM_H

#include <dolfin.h>

using namespace dolfin;

#warning "put in common defs"
typedef std::vector<std::pair<SubDomain*,Function*> > DirichletBCList;

class NO2Problem
{
public:
    NO2Problem(
            Mesh& mesh,
            Function* velocity,
            Function* SInitial,
            Function* NO2b,
            Function* NO2v,
            DirichletBCList dirichletBcs
            ) :
        mesh(mesh),
        velocity(velocity),
        SInitial(SInitial),
        NO2b(NO2b),
        NO2v(NO2v),
        dirichletBcs(dirichletBcs)
    {
        diffusivity = new Function(mesh,1e-2);
    };

    virtual ~NO2Problem ()
    {
        delete diffusivity;
    };

    Mesh& getMesh() const {return mesh;}
    DirichletBCList getDirichletBCs() const {return dirichletBcs;}
    Function* getVelocity() const {return velocity;}
    Function* getSInitial() const {return SInitial;}
    Function* getNO2b() const {return NO2b;}
    Function* getNO2v() const {return NO2v;}
    Function* getDiffusivity() const {return diffusivity;}

private:
    Mesh& mesh;
    Function* velocity;
    Function* SInitial;
    Function* NO2b;
    Function* NO2v;
    Function* diffusivity;
    DirichletBCList dirichletBcs;
};

#endif /* end of include guard: NO2PROBLEM_H */
