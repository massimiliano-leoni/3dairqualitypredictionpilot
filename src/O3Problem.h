/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef O3PROBLEM_H
#define O3PROBLEM_H

#include <dolfin.h>

using namespace dolfin;

#warning "put in common defs"
typedef std::vector<std::pair<SubDomain*,Function*> > DirichletBCList;

class O3Problem
{
public:
    O3Problem(
            Mesh& mesh,
            Function* velocity,
            Function* SInitial,
            Function* O3b,
            DirichletBCList dirichletBcs
            ) :
        mesh(mesh),
        velocity(velocity),
        SInitial(SInitial),
        O3b(O3b),
        dirichletBcs(dirichletBcs)
    {
        diffusivity = new Function(mesh,1);
    };

    virtual ~O3Problem ()
    {
        delete diffusivity;
    };

    Mesh& getMesh() const {return mesh;}
    DirichletBCList getDirichletBCs() const {return dirichletBcs;}
    Function* getVelocity() const {return velocity;}
    Function* getSInitial() const {return SInitial;}
    Function* getO3b() const {return O3b;}
    Function* getDiffusivity() const {return diffusivity;}

private:
    Mesh& mesh;
    Function* velocity;
    Function* SInitial;
    Function* O3b;
    Function* diffusivity;
    DirichletBCList dirichletBcs;
};

#endif /* end of include guard: O3PROBLEM_H */
