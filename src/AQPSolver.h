/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AQPSOLVER_H
#define AQPSOLVER_H

#include <dolfin.h>

#include "PrimalSolver.h"
#include "NOSolver.h"
#include "NO2Solver.h"
#include "O3Solver.h"

using namespace dolfin;

class AQPSolver
{
public:
    typedef WeakSlip SlipPolicy;
    typedef LocalTimeStep TimeStepPolicy;
    typedef DimensionalVelocityStabilization StabilizationPolicy;

    typedef PrimalSolver<SlipPolicy,
                         TimeStepPolicy,
                         StabilizationPolicy> NSESolver;

    AQPSolver(Mesh& mesh,
            DirichletBCList dbcs_m,
            DirichletBCList dbcs_c,
            DirichletBCList dbcs_dm,
            Function* u_i0,
            OutputQuantityList& outputQuantities,
            SlipPolicy::SlipBCData slipData,
            Function* f,
            real cfl_target,
            real nu,
            Function* NOb,
            Function* NOv,
            Function* NO2b,
            Function* NO2v,
            Function* O3b,
            Function* NOInitial,
            Function* NO2Initial,
            Function* O3Initial,
            DirichletBCList dbcs_s
            );
    virtual ~AQPSolver ();

    real getTFinal() const {return tFinal;}
    void setTFinal(real t);
    real getDt() const {return dt;}
    void setDt(real dt);
    uint getNoSamples() const {return nsSolver->getNoSamples();}
    void setNoSamples(const uint n);
    real getNSInitTime() const {return nsSolver->getInitTime();}
    void setNSInitTime(const real t) {nsSolver->setInitTime(t);}
    real getTime() const {return nsSolver->getTime();}

    real step();
    void run();
    void save();

private:
    NSESolver* nsSolver;
    NOProblem* noProblem;
    NOSolver* noSolver;
    NO2Problem* no2Problem;
    NO2Solver* no2Solver;
    O3Problem* o3Problem;
    O3Solver* o3Solver;

    real tFinal;
    real dt;
    real time;
    real nsTime;

    uint sample;
    uint nsteps;

    File* file_sol;
};

#endif /* end of include guard: AQPSOLVER_H */
