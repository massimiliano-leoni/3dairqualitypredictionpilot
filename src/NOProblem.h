/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOPROBLEM_H
#define NOPROBLEM_H

#include <dolfin.h>

using namespace dolfin;

#warning "put in common defs"
typedef std::vector<std::pair<SubDomain*,Function*> > DirichletBCList;

class NOProblem
{
public:
    NOProblem(
            Mesh& mesh,
            Function* velocity,
            Function* SInitial,
            Function* NOb,
            Function* NOv,
            DirichletBCList dirichletBcs
            ) :
        mesh(mesh),
        velocity(velocity),
        SInitial(SInitial),
        NOb(NOb),
        NOv(NOv),
        dirichletBcs(dirichletBcs)
    {
        diffusivity = new Function(mesh,1);
    };

    virtual ~NOProblem ()
    {
        delete diffusivity;
    };

    Mesh& getMesh() const {return mesh;}
    DirichletBCList getDirichletBCs() const {return dirichletBcs;}
    Function* getVelocity() const {return velocity;}
    Function* getSInitial() const {return SInitial;}
    Function* getNOb() const {return NOb;}
    Function* getNOv() const {return NOv;}
    Function* getDiffusivity() const {return diffusivity;}

private:
    Mesh& mesh;
    Function* velocity;
    Function* SInitial;
    Function* NOb;
    Function* NOv;
    Function* diffusivity;
    DirichletBCList dirichletBcs;
};

#endif /* end of include guard: NOPROBLEM_H */
