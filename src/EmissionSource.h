/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EMISSIONSOURCE_H
#define EMISSIONSOURCE_H

#include <dolfin/function/Function.h>
#include <dolfin/mesh/IntersectionDetector.h>

using namespace dolfin;

#include "AQPSolver.h"
#include "EmissionData.h"
#include "StreetMappingData.h"

class EmissionSource : public Function
{
public:
    EmissionSource(
            Mesh& mesh,
            StreetMappingData& streetMappingData,
            const EmissionData& emissionData,
            StreetsData& streetsData,
            real noxFraction = 1) :
        Function(mesh),
        streetMappingData(streetMappingData),
        emissionData(emissionData),
        streetsData(streetsData),
        noxFraction(noxFraction)
    {}

    void setSolver(const AQPSolver* s) {solver = s;}

    void eval(real* values, const real* x) const;

private:
    StreetMappingData& streetMappingData;
    const EmissionData& emissionData;
    StreetsData& streetsData;
    real noxFraction;
    const AQPSolver* solver;
};

#endif /* end of include guard: EMISSIONSOURCE_H */
