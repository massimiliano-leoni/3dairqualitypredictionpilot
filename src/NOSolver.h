/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOSOLVER_H
#define NOSOLVER_H

#include <dolfin.h>

#include "NOProblem.h"
#include "NO.h"
#include "FormArray.h"

using namespace dolfin;

class NOSolver
{
public:
    NOSolver(NOProblem* problem);
    virtual ~NOSolver ();

    void solveSystem();
    real step();
    void run();
    void save();

    real getDt() const {return dt;}
    void setDt(const real dt) {this->dt = dt; dt_f->init(problem->getMesh(),dt);}
    real getInitialTime() const {return tInitial;}
    void setInitialTime(const real t) {tInitial = t; this->t = t;}
    uint getNoSamples() const {return no_samples;}
    void setNoSamples(const uint n) {no_samples = n;}
    real getTFinal() const {return tFinal;}
    void setTFinal(const real t) {tFinal = t;}
    Function* getSolution() const {return S;}
#warning "why not const args?"
    void setO3 (Function* o3) {(*a)["O3"] = o3;}
    void setNO2 (Function* no2) {(*L)["NO2"] = no2;}

private:
    NOProblem* problem;

    Function* S;
    Function* SOld;
    NOBilinearForm* a;
    NOLinearForm* L;
    FormArray forms;
    LinearPDE* pde;
    File* file_S;

    MeshSize* h;

    Array<BoundaryCondition*> bcs;

    real tInitial;
    real tFinal;
    real t;
    real dt;
    Function* dt_f;

    uint no_samples;
    uint sample;
};

#endif /* end of include guard: NOSOLVER_H */
