/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WINDDATA_H
#define WINDDATA_H

#include <string>
#include <dolfin.h>

class WindData
{
public:
    WindData (std::string fx, std::string fy);
    virtual ~WindData () {};

    std::vector<dolfin::real> eval(const dolfin::real t, const dolfin::real z) const;

private:
    dolfin::real groundHeight;

    std::vector<dolfin::real> timePoints;
    std::vector<dolfin::real> zPoints;
#warning "Should use a contiguous space?"
    std::vector<std::vector<std::pair<dolfin::real,dolfin::real> > > data;
};

#endif /* end of include guard: WINDDATA_H */
