/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "NOSolver.h"

NOSolver::NOSolver(NOProblem* problem) :
    problem(problem),
    no_samples(100),
    sample(1)
{
    Mesh& mesh = problem->getMesh();

    const DirichletBCList& dirbcs = problem->getDirichletBCs();
    for (int i = 0; i < dirbcs.size(); i++)
    {
        bcs.push_back(new DirichletBC(*dirbcs[i].second,
                      mesh,
                      *dirbcs[i].first));
    }

    S = new Function(*problem->getSInitial());
    SOld = new Function(*problem->getSInitial());
    dt = 0.01;
    dt_f = new Function(mesh,dt);
    h = new MeshSize(mesh);

    a = new NOBilinearForm(mesh,forms);
    (*a)["vel"] = problem->getVelocity();
    (*a)["diffusivity"] = problem->getDiffusivity();
    (*a)["dt"] = dt_f;
    (*a)["h"] = h;

    L = new NOLinearForm(mesh,forms);
    (*L)["uOld"] = SOld;
    (*L)["NOb"] = problem->getNOb();
    (*L)["NOv"] = problem->getNOv();

    pde = new LinearPDE(*a, *L, mesh, bcs);

    tInitial = 0;
    t = tInitial;
    tFinal = 1;

    file_S = new File("NO.bin");
}

void NOSolver::solveSystem()
{
    pde->solve(*S);
}

real NOSolver::step()
{
    solveSystem();

    t += dt;

    *SOld = *S;

    return t;
}

void NOSolver::run()
{
    while(t <= tFinal)
    {
        step();

        if(t >= tFinal*(real(sample)/real(no_samples)))
        {
            cout << "Saving NO at time " << t << endl;
            save();
            sample++;
        }
    }
}

void NOSolver::save()
{
    *file_S << *S;
}

NOSolver::~NOSolver ()
{
    bcs.clear();
    delete S;
    delete SOld;
    delete a;
    delete L;
    delete pde;
    delete file_S;
    delete h;
    delete dt_f;
}
