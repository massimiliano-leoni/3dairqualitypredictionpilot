/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <limits>

#include "StreetsData.h"

using namespace std;

StreetsData::StreetsData(string f_s)
{
    ifstream f(f_s.c_str());

    string str;
    istringstream iss(str);
    getline(f, str);
    getline(f, str);
    getline(f, str);
    getline(f, str);
    getline(f, str);

    dolfin::uint id;
    while (getline(f,str))
    {
        iss.str(str);
        iss.clear();
        iss >> id;
        data[id] = vector<dolfin::real>(4);
        iss >> data[id][StartX]
            >> data[id][StartY]
            >> data[id][EndX]
            >> data[id][EndY];
    }

    f.close();
}

