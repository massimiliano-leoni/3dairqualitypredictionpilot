/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STREETSDATA_H
#define STREETSDATA_H

#include <map>
#include <vector>

#include <dolfin.h>

class StreetsData
{
public:
    StreetsData (std::string f);
    virtual ~StreetsData () {};

    typedef std::map<dolfin::uint,std::vector<dolfin::real> > data_t;
    typedef typename data_t::iterator iterator;
    iterator begin() {return data.begin();}
    iterator end() {return data.end();}
    data_t::mapped_type operator[](const data_t::key_type k) {return data[k];}

    enum fields {
        StartX = 0,
        StartY,
        EndX,
        EndY};

private:
#warning "Should use a contiguous space?"
    std::map<dolfin::uint,std::vector<dolfin::real> > data;
};

#endif /* end of include guard: STREETSDATA_H */
