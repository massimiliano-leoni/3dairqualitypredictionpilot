/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STREETMAPPINGDATA_H
#define STREETMAPPINGDATA_H

#include <string>
#include <vector>
#include <dolfin.h>

#include "StreetsData.h"

class StreetMappingData
{
public:
    StreetMappingData(dolfin::Mesh& mesh,
            const dolfin::SubDomain& ground,
            StreetsData& streetsData);
    virtual ~StreetMappingData() {}

#warning "Marking non-const and using operator[] to get empty map for cells that don't intersect the road"
    const std::map<dolfin::uint,dolfin::real>
        getCellIntersections(const dolfin::uint index) {
            return intersections[index];}

private:
    dolfin::Point intersectSegments(
            const dolfin::Point p0,
            const dolfin::Point p1,
            const dolfin::Point p2,
            const dolfin::Point p3) const;

    std::map<dolfin::uint,std::map<dolfin::uint,dolfin::real> > intersections;
};

#endif /* end of include guard: STREETMAPPINGDATA_H */
