/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WINDINFLOW_H
#define WINDINFLOW_H

#include <dolfin/function/Function.h>

class WindData;
class AQPSolver;

using namespace dolfin;

class WindInflow : public Function
{
public:
    WindInflow(Mesh& mesh, WindData& wd) :
        Function(mesh),
        windData(wd)
    {}

    void setSolver(const AQPSolver* s) {solver = s;}

    void eval(real* values, const real* x) const;

private:
    WindData& windData;
    const AQPSolver* solver;
};

#endif /* end of include guard: WINDINFLOW_H */
