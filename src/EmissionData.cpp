/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <limits>

#include "EmissionData.h"

using namespace std;

EmissionData::EmissionData(string f_s)
{
    ifstream f(f_s.c_str());

    string str;
    istringstream iss(str);
    getline(f, str);
    while (str.substr(0,3) != "###")
    {
        getline(f, str);
    }
    iss.str(str);
    iss.clear();
    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');

    size_t nTimePoints;
    iss >> nTimePoints;
    nTimePoints--;

    getline(f, str);

    dolfin::real t;
    iss.str(str);
    iss.clear();
    iss.ignore(numeric_limits<streamsize>::max(),'\t');
    timePoints.reserve(nTimePoints);
    for (size_t i = 0; i < nTimePoints; i++)
    {
        iss >> t;
        timePoints.push_back(t);
    }

    dolfin::uint id;
    while (getline(f,str))
    {
        iss.str(str);
        iss.clear();
        iss >> id;
        data[id] = vector<dolfin::real>(nTimePoints);
        for (size_t i = 0; i < nTimePoints; i++)
        {
            iss >> data[id][i];
            data[id][i] /= 1e9;
        }
    }

    f.close();
}

dolfin::real EmissionData::eval(const dolfin::uint id, const dolfin::real t) const
{
    dolfin::uint tpos = 0;
    dolfin::real lt;

    if (t < timePoints.front())
    {
        lt = 0;
        tpos = 0;
    }
    else if (t > timePoints.back())
    {
        lt = 1;
        tpos = timePoints.size()-2;
    }
    else
    {
        while (timePoints[tpos+1] < t)
        {
            tpos++;
        }
        lt = (t-timePoints[tpos])/(timePoints[tpos+1]-timePoints[tpos]);
    }

    std::map<dolfin::uint,std::vector<dolfin::real> >::const_iterator it = data.find(id);
    return it->second[tpos] + lt*(it->second[tpos+1]-it->second[tpos]);
}
