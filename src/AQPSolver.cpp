/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "AQPSolver.h"

AQPSolver::AQPSolver(
        Mesh& mesh,
        DirichletBCList dbcs_m,
        DirichletBCList dbcs_c,
        DirichletBCList dbcs_dm,
        Function* u_i0,
        OutputQuantityList& outputQuantities,
        SlipPolicy::SlipBCData slipData,
        Function* f,
        real cfl_target,
        real nu,
        Function* NOb,
        Function* NOv,
        Function* NO2b,
        Function* NO2v,
        Function* O3b,
        Function* NOInitial,
        Function* NO2Initial,
        Function* O3Initial,
        DirichletBCList dbcs_s
        ):
    dt(60),
    time(0),
    nsTime(0),
    sample(1),
    nsteps(1)
{
    nsSolver = new NSESolver(
            mesh,
            dbcs_m,
            dbcs_c,
            dbcs_dm,
            slipData,
            u_i0,
            outputQuantities,
            NULL,
            NULL,
            NULL,
            f,
            cfl_target,
            nu
            );

    noProblem = new NOProblem(
            mesh,
            nsSolver->getU(),
            NOInitial,
            NOb,
            NOv,
            dbcs_s);
    noSolver = new NOSolver(noProblem);

    no2Problem = new NO2Problem(
            mesh,
            nsSolver->getU(),
            NO2Initial,
            NO2b,
            NO2v,
            dbcs_s);
    no2Solver = new NO2Solver(no2Problem);

    o3Problem = new O3Problem(
            mesh,
            nsSolver->getU(),
            O3Initial,
            O3b,
            dbcs_s);
    o3Solver = new O3Solver(o3Problem);

    noSolver->setO3(o3Solver->getSolution());
    noSolver->setNO2(no2Solver->getSolution());
    no2Solver->setNO(noSolver->getSolution());
    no2Solver->setO3(o3Solver->getSolution());
    o3Solver->setNO(noSolver->getSolution());
    o3Solver->setNO2(no2Solver->getSolution());

    nsSolver->setNoSamples(1440);
    noSolver->setNoSamples(1440);
    no2Solver->setNoSamples(1440);
    o3Solver->setNoSamples(1440);

    file_sol = new File("aqpSolution.bin");
}

AQPSolver::~AQPSolver()
{
    delete nsSolver;
    delete noProblem;
    delete noSolver;
    delete no2Problem;
    delete no2Solver;
    delete o3Problem;
    delete o3Solver;
    delete file_sol;
}

void AQPSolver::setTFinal(const real t)
{
    tFinal = t;
    nsSolver->setT(tFinal);
    noSolver->setTFinal(tFinal);
    no2Solver->setTFinal(tFinal);
    o3Solver->setTFinal(tFinal);
}

void AQPSolver::setDt(real dt)
{
    this->dt = dt;
    noSolver->setDt(dt);
    no2Solver->setDt(dt);
    o3Solver->setDt(dt);
}

void AQPSolver::save()
{
    std::vector<std::pair<Function*, std::string> > output;
    std::pair<Function*, std::string> u_out(nsSolver->getU(), "Velocity");
    std::pair<Function*, std::string> p_out(nsSolver->getP(), "Pressure");
    std::pair<Function*, std::string> no_out(noSolver->getSolution(), "NO");
    std::pair<Function*, std::string> no2_out(no2Solver->getSolution(), "NO2");
    std::pair<Function*, std::string> o3_out(o3Solver->getSolution(), "O3");
    output.push_back(u_out);
    output.push_back(p_out);
    output.push_back(no_out);
    output.push_back(no2_out);
    output.push_back(o3_out);

    *this->file_sol << output;
}

real AQPSolver::step()
{
    while (nsTime < nsteps*dt)
    {
        nsTime = nsSolver->step();
    }
    nsteps++;

    cout << "Solving NO" << endl;
    noSolver->step();

    cout << "Solving NO2" << endl;
    no2Solver->step();

    cout << "Solving O3" << endl;
    o3Solver->step();

    time += nsTime-time;

    if (time >= tFinal*(real(sample)/getNoSamples()))
    {
        cout << "saving solution at time " << time << endl;
        save();
        sample++;
    }
}

void AQPSolver::run()
{
    while (time < tFinal)
    {
        step();
    }
}

void AQPSolver::setNoSamples(const uint n)
{
    nsSolver->setNoSamples(n);
    noSolver->setNoSamples(n);
    no2Solver->setNoSamples(n);
    o3Solver->setNoSamples(n);
}
