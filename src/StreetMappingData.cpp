/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "StreetMappingData.h"

#include <deque>

#include <dolfin/mesh/IntervalCell.h>
#include <dolfin/mesh/TriangleCell.h>

using namespace dolfin;

StreetMappingData::StreetMappingData(Mesh& mesh,
        const SubDomain& ground,
        StreetsData& streetsData)
{
    MeshFunction<uint> groundMarker(mesh,mesh.topology().dim()-1);
    groundMarker = 0;
    ground.mark(groundMarker,1);

    for (FacetIterator f(mesh); !f.end(); ++f)
    {
        if (groundMarker(*f))
        {
            const uint cellIdx = f->entities(3)[0];
            std::map<uint,real> fractions;

            TriangleCell tri;
            std::deque<dolfin::real> oldZ;
            for (VertexIterator v(*f); !v.end(); ++v)
            {
                oldZ.push_back(v->x()[2]);
                v->x()[2] = 0;
            }

            // Get mesh geometry
            const MeshGeometry& geometry = f->mesh().geometry();

            for (StreetsData::iterator s = streetsData.begin();
                    s != streetsData.end(); ++s)
            {
                real factor = 0;
                Point P0(s->second[StreetsData::StartX],s->second[StreetsData::StartY]);
                Point P1(s->second[StreetsData::EndX],s->second[StreetsData::EndY]);
                if (tri.intersects(*f,P0,P1))
                {
                    if (tri.intersects(*f,P0) and tri.intersects(*f,P1))
                    {
                        fractions[s->first] = 1;
                    }
                    else if (tri.intersects(*f,P0))
                    {
                        IntervalCell i;
                        for (EdgeIterator e(*f); !e.end(); ++e)
                        {
                            if (i.intersects(*e,P0,P1))
                            {
                                const MeshGeometry& geometry = e->mesh().geometry();
                                const uint* vertices = e->entities(0);
                                const Point x0 = geometry.point(vertices[0]);
                                const Point x1 = geometry.point(vertices[1]);

                                Point Pint = intersectSegments(x0,x1,P0,P1);
                                fractions[s->first] = Pint.distance(P0)/P0.distance(P1);

                                break;
                            }
                        }
                    }
                    else if (tri.intersects(*f,P1))
                    {
                        IntervalCell i;
                        for (EdgeIterator e(*f); !e.end(); ++e)
                        {
                            if (i.intersects(*e,P0,P1))
                            {
                                const MeshGeometry& geometry = e->mesh().geometry();
                                const uint* vertices = e->entities(0);
                                const Point x0 = geometry.point(vertices[0]);
                                const Point x1 = geometry.point(vertices[1]);

                                Point Pint = intersectSegments(x0,x1,P0,P1);
                                fractions[s->first] = Pint.distance(P1)/P0.distance(P1);

                                break;
                            }
                        }
                    }
                    else
                    {
                        IntervalCell i;
                        std::vector<Point> intersectionPoints;
                        for (EdgeIterator e(*f); !e.end(); ++e)
                        {
                            if (i.intersects(*e,P0,P1))
                            {
                                const MeshGeometry& geometry = e->mesh().geometry();
                                const uint* vertices = e->entities(0);
                                const Point x0 = geometry.point(vertices[0]);
                                const Point x1 = geometry.point(vertices[1]);

                                intersectionPoints.push_back(intersectSegments(x0,x1,P0,P1));
                            }
                        }

                        fractions[s->first] = intersectionPoints[0].distance(intersectionPoints[1])/P0.distance(P1);
                    }
                }
            }

            intersections[cellIdx] = fractions;

            for (VertexIterator v(*f); !v.end(); ++v)
            {
                v->x()[2] = oldZ[0];
                oldZ.pop_front();
            }
        }
    }
}

Point StreetMappingData::intersectSegments(const Point p0,
                                           const Point p1,
                                           const Point p2,
                                           const Point p3) const
{
    const real xint = (
            (p0[0]*p1[1]-p0[1]*p1[0])*(p2.x()-p3.x()) - (p0[0]-p1[0])*(p2.x()*p3.y()-p2.y()*p3.x()))
        /(
                (p0[0]-p1[0])*(p2.y()-p3.y()) - (p0[1]-p1[1])*(p2.x()-p3.x()));
    const real yint = (
            (p0[0]*p1[1]-p0[1]*p1[0])*(p2.y()-p3.y()) - (p0[1]-p1[1])*(p2.x()*p3.y()-p2.y()*p3.x()))
        /(
                (p0[0]-p1[0])*(p2.y()-p3.y()) - (p0[1]-p1[1])*(p2.x()-p3.x()));

    return Point(xint,yint);
}
