/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <limits>

#include "WindData.h"

using namespace std;

WindData::WindData(string fx_s, string fy_s)
{
    ifstream fx(fx_s.c_str());
    ifstream fy(fy_s.c_str());

    string strx;
    string stry;
    istringstream iss(strx);
    istringstream iss2(stry);
    getline(fx, strx);
    getline(fy, stry);
    while (strx.substr(0,6) != "Ground")
    {
        getline(fx, strx);
        getline(fy, stry);
    }
    iss.str(strx);
    iss.clear();

    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');

    iss >> groundHeight;

    while (strx.substr(0,3) != "###")
    {
        getline(fx, strx);
        getline(fy, stry);
    }
    iss.str(strx);
    iss.clear();
    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');
    iss.ignore(numeric_limits<streamsize>::max(),' ');

    size_t nTimePoints;
    iss >> nTimePoints;
    nTimePoints--;

    getline(fx, strx);
    getline(fy, stry);

    dolfin::real t;
    iss.str(strx);
    iss.clear();
    iss.ignore(numeric_limits<streamsize>::max(),'\t');
    for (size_t i = 0; i < nTimePoints; i++)
    {
        iss >> t;
        timePoints.push_back(t);
    }

    dolfin::real z;
    while (getline(fx,strx))
    {
        getline(fy, stry);
        iss.str(strx);
        iss.clear();
        iss2.str(stry);
        iss2.clear();
        iss >> z;
        zPoints.push_back(z+groundHeight);
        iss2.ignore(numeric_limits<streamsize>::max(),'\t');

        data.push_back(vector<pair<dolfin::real,dolfin::real> >());
        for (size_t i = 0; i < nTimePoints; i++)
        {
            dolfin::real x;
            dolfin::real y;
            iss >> x;
            iss2 >> y;
            data.back().push_back(make_pair(x,y));
        }
    }

    fx.close();
    fy.close();
}

vector<dolfin::real> WindData::eval(const dolfin::real t, const dolfin::real z) const
{
    dolfin::uint tpos = 0;
    dolfin::uint zpos = 0;
    dolfin::real lt;
    dolfin::real lz;
    vector<dolfin::real> result(3);

    if (t < timePoints.front())
    {
        lt = 0;
        tpos = 0;
    }
    else if (t > timePoints.back())
    {
        lt = 1;
        tpos = timePoints.size()-2;
    }
    else
    {
        while (timePoints[tpos+1] < t)
        {
            tpos++;
        }
        lt = (t-timePoints[tpos])/(timePoints[tpos+1]-timePoints[tpos]);
    }

    if (z < zPoints.front())
    {
        lz = 0;
        zpos = 0;
    }
    else if (z > zPoints.back())
    {
        lz = 1;
        zpos = zPoints.size()-2;
    }
    else
    {
        while (zPoints[zpos+1] < z)
        {
            zpos++;
        }
        lz = (z-zPoints[zpos])/(zPoints[zpos+1]-zPoints[zpos]);
    }

    result[0] = data[zpos][tpos].first
                + lt*(data[zpos][tpos+1].first-data[zpos][tpos].first)
                + lz*(data[zpos+1][tpos].first-data[zpos][tpos].first);
    result[1] = data[zpos][tpos].second
                + lt*(data[zpos][tpos+1].second-data[zpos][tpos].second)
                + lz*(data[zpos+1][tpos].second-data[zpos][tpos].second);
    result[2] = 0;

    return result;
}
