/*
 * Copyright 2018 Massimiliano Leoni <leoni.massimiliano1@gmail.com>
 *
 * This file is part of 3DAirQualityPredictionPilot.
 *
 * 3DAirQualityPredictionPilot is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 3DAirQualityPredictionPilot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with 3DAirQualityPredictionPilot.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "EmissionSource.h"

void EmissionSource::eval(real* values, const real* x) const
{
    values[0] = 0;
    const std::map<dolfin::uint,dolfin::real>& intersections =
        streetMappingData.getCellIntersections(cell().index());
    for (std::map<dolfin::uint,dolfin::real>::const_iterator it = intersections.begin();
            it != intersections.end();
            it++)
    {
        typename StreetsData::data_t::mapped_type v = streetsData[it->first];
        Point P0(v[StreetsData::StartX],v[StreetsData::StartY]);
        Point P1(v[StreetsData::EndX],v[StreetsData::EndY]);
        values[0] += P0.distance(P1)
                     * emissionData.eval(it->first,solver->getTime())
                     * it->second;
    }
    values[0] *= noxFraction/cell().volume()*solver->getDt();
}
